// eslint-disable-next-line react/prop-types
const ModalBody = ({ children }) => {
	return (
		<div className="modal-content">{children}</div>
	)
}

export default ModalBody