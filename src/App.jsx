/* eslint-disable no-unused-vars */
import { useState } from 'react'
import LauncherModal from './components/LauncherModal/LauncherModal'
import ModalImage from './components/ModalImage/ModalImage'
import ModalText from './components/ModalText/ModalText'
import './App.css'

const App = (props) => {
	const [isModalImage,setIsModalImage] = useState(false)
	const [isModalText, setIsModalText] = useState(false)
	
	const handleModalImage = () => setIsModalImage(!isModalImage)
	const handleModalText = () => setIsModalText(!isModalText)

	return (
		<>
			<LauncherModal
				handleModalImage={handleModalImage}
				handleModalText={handleModalText}
			/>
			{isModalImage && <ModalImage
				// isOpen={isModalImage}
				handleClose={handleModalImage}
				title='Product Delete!'
				desc='By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.'
			/>
			}
			{isModalText && <ModalText
				handleClose={handleModalText}
				title='Add Product “NAME”'
				desc='Description for you product'
			/>
			}
		</>
	)
}

export default App
