/* eslint-disable react/prop-types */
import ButtonWrapper from "../ButtonWrapper/ButtonWrapper";
import './LauncherModal.scss';

const LauncherModal = ({handleModalImage, handleModalText} ) => {
	return (
		<div className="launcher-modal">
			<ButtonWrapper
				firstClick={handleModalImage}
				secondaryClick={handleModalText}
				firstClassName="btn-opener"
				secondaryClassName="btn-opener"
				firstText="Open first modal"
				secondaryText="Open second modal" />
		</div>
	)
}

export default LauncherModal