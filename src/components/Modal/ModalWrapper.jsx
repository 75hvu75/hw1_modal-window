import './Modal.scss'

// eslint-disable-next-line react/prop-types
const ModalWrapper = ({ children, handleClose }) => {
	const handleModalWrapper = (e) => {
    if (e.target === e.currentTarget) {
      handleClose();
    }
  };
	return (
		<div className="modal-wrapper" onClick={handleModalWrapper}>{children}</div>
	)
}

export default ModalWrapper