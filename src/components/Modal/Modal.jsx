import cx from "classnames"

// eslint-disable-next-line react/prop-types
const Modal = ({children, classNames}) => {
	return (
		<div className={cx("modal", classNames)}>
			<div className={cx("modal-box", classNames)}>{children}</div>
		</div>
	)
}

export default Modal