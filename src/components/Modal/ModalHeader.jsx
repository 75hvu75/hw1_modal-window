// eslint-disable-next-line react/prop-types
const ModalHeader = ({children})=>{
	return(
		<div className="modal-header">{children}</div>
	)
}

export default ModalHeader