/* eslint-disable react/prop-types */
import Button from "../Button/Button"

const ButtonWrapper = ({
	firstText,
	secondaryText,
	firstClassName,
	secondaryClassName,
	firstClick,
	secondaryClick
}) => {
	return (
		<div className="button-wrapper">
			{firstText && <Button classNames={firstClassName} onclick={firstClick}>{firstText}</Button>}
			{secondaryText && <Button classNames={secondaryClassName} onclick={secondaryClick}>{secondaryText}</Button>}
		</div>
	)
}

export default ButtonWrapper