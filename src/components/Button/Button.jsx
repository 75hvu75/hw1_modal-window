/* eslint-disable react/prop-types */
import cx from 'classnames'
import './Button.scss'

const Button = (props) => {
	const { type = 'button', classNames, onclick, children, ...restProps } = props
	return (
		<button type={type} className={cx(`btn ${classNames}`)} onClick={onclick} {...restProps}>{children}</button>)
}

export default Button