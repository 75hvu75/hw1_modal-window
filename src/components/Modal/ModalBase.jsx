/* eslint-disable react/prop-types */
import ModalBody from "./ModalBody"
import ModalFooter from "./ModalFooter"
import ModalHeader from "./ModalHeader"
import ModalClose from "./ModalClose"
import Modal from "./Modal"
import ModalWrapper from "./ModalWrapper"

const ModalBase = ({title, desc, handleOk, handleClose}) => {
	return (
		<ModalWrapper>
			<Modal>
				<ModalClose click={handleClose} />
				<ModalHeader />
				<ModalBody>
					<h2>{title}</h2>
					<p>{desc}</p>
				</ModalBody>
				<ModalFooter firstText="CANCEL" secondaryText="DELETE" clickFirst={handleOk} clickSecondary={handleClose}/>
			</Modal>
		</ModalWrapper>
	)
}
export default ModalBase