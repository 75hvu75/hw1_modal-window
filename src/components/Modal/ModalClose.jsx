// eslint-disable-next-line react/prop-types
const ModalClose = ({ onclick }) => {
	return (
		<button type="button" className="modal-close" onClick={onclick}>
			<svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M15 1L1 15M15 15L1 1.00001" stroke="#3c4242" strokeWidth="1.5" strokeLinecap="round" />
			</svg>
		</button>
	)
}

export default ModalClose