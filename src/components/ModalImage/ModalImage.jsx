/* eslint-disable react/prop-types */
import ModalWrapper from '../Modal/ModalWrapper';
import Modal from '../Modal/Modal';
import ModalClose from '../Modal/ModalClose';
import ModalHeader from '../Modal/ModalHeader';
import ModalBody from '../Modal/ModalBody';
import ModalFooter from '../Modal/ModalFooter'
import ButtonWrapper from '../ButtonWrapper/ButtonWrapper';

const ModalImage = ({ title, desc, handleClose }) => {
	return (
		<ModalWrapper handleClose={handleClose}>
			<Modal>
				<ModalClose onclick={handleClose} />
				<ModalHeader />
				<ModalBody>
					<h2 className='title'>{title}</h2>
					<p className='desc'>{desc}</p>
				</ModalBody>
				<ModalFooter>
					<ButtonWrapper
						firstClassName="btn-cancel"
						secondaryClassName="btn-delete"
						firstText="NO, CANCEL"
						secondaryText="YES, DELETE"
					/>
				</ModalFooter>
			</Modal>
		</ModalWrapper>
	)
}

export default ModalImage