/* eslint-disable react/prop-types */
const ModalFooter = ({children}) => {
	return (
		<div className="modal-footer">{children}</div>
	)
}

export default ModalFooter