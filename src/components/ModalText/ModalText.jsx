/* eslint-disable react/prop-types */
import ModalWrapper from '../Modal/ModalWrapper';
import Modal from '../Modal/Modal';
import ModalClose from '../Modal/ModalClose';
import ModalBody from '../Modal/ModalBody';
import ModalFooter from '../Modal/ModalFooter'
import ButtonWrapper from '../ButtonWrapper/ButtonWrapper';

const ModalText = ({ title, desc, handleClose }) => {
	return (
		<ModalWrapper handleClose={handleClose}>
			<Modal>
				<ModalClose onclick={handleClose} />
				<ModalBody>
					<h2 className='title'>{title}</h2>
					<p className='desc'>{desc}</p>
				</ModalBody>
				<ModalFooter>
					<ButtonWrapper
						firstClassName="btn-cancel"
						firstText="ADD TO FAVORITE"
					/>
				</ModalFooter>
			</Modal>
		</ModalWrapper>
	)
}

export default ModalText